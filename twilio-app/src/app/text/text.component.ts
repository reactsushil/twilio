import { Component, OnInit, Input } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { TextService } from '../services/text.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-text',
  templateUrl: './text.component.html',
  styleUrls: ['./text.component.scss']
})
export class TextComponent implements OnInit {
  @Input() public isUserLoggedIn: boolean;

  smsForm: FormGroup;
  submitted = false;
  constructor( private formBuilder: FormBuilder, private textService: TextService,
    private toastrService: ToastrService ) { }

  ngOnInit(): void {
    this.smsForm = this.formBuilder.group({
      countryCode: ['', Validators.required],
      number: ['', Validators.required],
      textMsg: ['', Validators.required],
    });
  }

  get f() {
    return this.smsForm.controls;
  }

  sendSms(){
    this.submitted = true;
    if(this.smsForm.invalid){
      return;
    }
    this.textService.sendSms(this.smsForm.value).subscribe((result) => {
      if(result.status == 1){
        this.toastrService.error(`${result.msg}`, 'Error');
        return;
      }
      this.toastrService.success(`${result.msg}`, 'Success');
      this.submitted = false;
      this.smsForm.reset();
    },
    (err) => {
      console.log("********Error in sendSms*******", err);
    });
  }

}
