import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { TextComponent } from './text/text.component';
import { EmailComponent } from './email/email.component';
import { MainComponent } from './main/main.component';

const routes: Routes = [
  { path: '', component: LoginComponent},
  { path: 'home', component: MainComponent,
    children: [
      { path: 'text', component: TextComponent  },
      { path: 'email', component: EmailComponent  },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
