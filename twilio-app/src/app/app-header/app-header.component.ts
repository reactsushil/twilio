import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-app-header',
  templateUrl: './app-header.component.html',
  styleUrls: ['./app-header.component.scss']
})
export class AppHeaderComponent implements OnInit {
  @Input() public title: string;
  @Input() public isUserLoggedIn: boolean;
  userName: string;
  constructor(private router: Router) { }

  ngOnInit(): void {
    const userDoc = window.sessionStorage.getItem('user-detail');
    this.userName = JSON.parse(userDoc).userid;
  }

  logout() {
    window.sessionStorage.clear();
    this.router.navigate([''])
  }

}
