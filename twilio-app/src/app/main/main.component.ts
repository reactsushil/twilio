import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  @Input() public title: string;
  @Input() public isUserLoggedIn: boolean;

  public menu: any = [
    {
      id: 1,
      name: "Text Msg"
    },
    {
      id: 2,
      name: "Email"
    }
  ]
  constructor() { }

  ngOnInit(): void {
  }

  changeMenu(id: number) {

  }

}
