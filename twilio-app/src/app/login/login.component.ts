import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  isNotLogin = true;
  userForm: FormGroup;
  submitted = false;
  constructor(private formBuilder: FormBuilder, private router: Router,
     private toastrService: ToastrService) { }

  ngOnInit(): void {
    this.userForm = this.formBuilder.group({
      userid: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  get f() {
    return this.userForm.controls;
  }

  login() {
    window.sessionStorage.clear();
    this.submitted = true;
    if(this.userForm.invalid){
      return;
    }
    window.sessionStorage.setItem('user-detail', JSON.stringify(this.userForm.value));
    this.toastrService.success('Suscessfully Loged In', 'Success');
    this.router.navigate(['home']);
  }
}
