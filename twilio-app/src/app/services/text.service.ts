import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { apiURL } from '../../environments/environment'
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TextService {

  constructor( private http: HttpClient ) { }

  public sendSms(textDoc: any): Observable<any> {
    return this.http.post(`${apiURL}/sendSms`, textDoc).pipe(
      map((res: any) => {
        return res;
      })
    );
  }
}
