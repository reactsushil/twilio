/**
 * token and accountSid will also get from environment(provide by twilio)
 */
const config = require("./config/config")
const client = require('twilio')(config.accountSid, config.token);

const sendSms = async (req, res) => {

  try{
    return new Promise((resolve, reject) => {
      client.messages
      .create({
            to: req.body.toNumber,
            from: config.fromNumber, // or get for environment
            body: req.body.textMsg
        }, (err, data) => {
            if (err){
              resolve(false);
                
            }
            resolve(true);
        })
    })
  }catch (e) {
    return false;

  }

}

module.exports = {
    sendSms
  };