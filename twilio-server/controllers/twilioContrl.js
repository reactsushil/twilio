const twilioService = require('../twilio');

const sendSms = async (req, res) => {
    const toNumber = req.body.countryCode + req.body.number;
    req.body.toNumber = toNumber;
    const smsDoc = await twilioService.sendSms(req, res);
    if (!smsDoc){
       return res.send({msg: "Error in send sms", status: 1});
    }

    return res.send({msg: "Sms send successfully", status: 0});
}

module.exports = {sendSms};