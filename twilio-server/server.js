const express = require("express");
const cors = require('cors');
const app = express();
const port = process.env.port || 5000;
const bodyParser = require('body-parser');
app.use(cors());
app.use(bodyParser.json());
require('./routes/routes')(app);

app.get('/testTwilio', (req, res) => {
  
});

app.listen(port, ()=> {
    console.log(`Running on ${port}`);
})